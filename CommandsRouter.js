const express = require("express");
const { spawn } = require('child_process');

const router = express.Router();

function responseCallback(err, message) {
    res.setHeader("Content-type", "application/json");
    if (err) {
        res.status(504);
        res.write(JSON.stringify({ message: "Gateway timeout, please check the load balancing system status" }));
    } else
        res.write(message);
    res.end();
}

function spawnNewJob(cb, playbookName, host) {
    const command = spawn(`sudo ansible-playbook adhoc/${playbookName} --extra-vars host=${host}` ,{
        cwd: __dirname,
        shell: true
    });
    command.on("error", (err) => {
        // cb(err, null);
        console.error(error);
    });
    command.stdout.on("data", msg => {
        console.log(msg.toString());
    });

    command.on("exit", (code) => {
        if(code === 0) {
            cb(true, "Operation Completed Successfully")
        } else {
            cb(false, "Operation Failed");
        }
    });
}

router.use((req, res, next) => {
    console.log("Router")
    next();
})

router.post("/start", (req, res, next) => {
    const host = req.body['ipAddress']['address'];
    const cb = (successd, message) => {
        if(successd)
            res.send(message);
        else {
            res.status(504);
            res.send(message);
        }
    };
    spawnNewJob(cb, "start.yml", host);
});

router.post("/stop", (req, res, next) => {
    const host = req.body['ipAddress']['address'];
    const cb = (successd, message) => {
        if(successd)
            res.send(message);
        else {
            res.status(504);
            res.send(message);
        }
    };
    spawnNewJob(cb, "stop.yml", host);
});
router.post("/install", (req, res, next) => {
    const host = req.body['ipAddress']['address'];
    const cb = (successd, message) => {
        if(successd)
            res.send(message);
        else {
            res.status(504);
            res.send(message);
        }
    };
    spawnNewJob(cb, "install.yml", host);
});
router.post("/uninstall", (req, res, next) => {
    const host = req.body['ipAddress']['address'];
    const cb = (successd, message) => {
        if(successd)
            res.send(message);
        else {
            res.status(504);
            res.send(message);
        }
    };
    spawnNewJob(cb, "uninstall.yml", host);
});
router.post("/reload", (req, res, next) => {
    const host = req.body['ipAddress']['address'];
    const cb = (successd, message) => {
        if(successd)
            res.send(message);
        else {
            res.status(504);
            res.send(message);
        }
    };
    spawnNewJob(cb, "reload.yml", host);
});

module.exports = router;



