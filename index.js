const express = require("express");
const cors = require("cors");
const ejs = require("ejs");
const fs = require("fs").promises;
const config = require("./config");

const commandRouter = require("./CommandsRouter");
const { synchronizeConfiguration, getServerStatistics } = require("./utils");

const app = express();
app.use(cors());
app.use(express.json());
app.use("/", (req, res, next) => {
    console.log(`${req.method} ${req.path}`);
    next();
});

app.get("/", (req, res, next) => {
    res.send("Welcome to express");
});

app.post("/sync", async (req, res, next) => {
    const addressInfo = req.body.portNum;
    const serverConfig = req.body.formData;
    try {
        const configurationString = await ejs.renderFile(__dirname + "/templates/main.ejs", { addressInfo, serverConfig }, {});
        await fs.writeFile(__dirname + "/ansible-nginx-role/files/main.conf", configurationString)
        const command = synchronizeConfiguration();

        command.on("close", (code) => {
            if (code === 0)
                res.end("configuration successfuly applied");
            else {
                res.status(400);
                res.end(`
                    A Problem occured when synchronizing configurations.
                    Please make sure configurations are correct.
                `);
            }
        });
    } catch (error) { console.error(error); }
});

app.get("/statistics", (req, res, next) => {
    getServerStatistics((err, statistics) => {
        res.setHeader("Content-type", "application/json");
        if (err) {
            res.status(504);
            res.write(JSON.stringify({ message: "Gateway timeout, please check the load balancing system status" }));
        } else
            res.write(JSON.stringify(statistics));
        res.end();
    });
});
app.use("/command", commandRouter);

app.listen(config["PORT"]);