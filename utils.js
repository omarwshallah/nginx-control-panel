const { spawn } = require("child_process");

function sync(host="127.0.0.1") {
    const command = spawn(`sudo ansible-playbook role-starter.yml --extra-vars host=${host}` ,{
        stdio: "ignore",
        cwd: __dirname,
        shell: true
    });
    return command;
}

function getStats(cb, host="127.0.0.1") {
    const command = spawn(`sudo ansible-playbook getServerStats.yml --extra-vars host=${host}` ,{
        cwd: __dirname,
        shell: true
    });
    command.on("error", (err) => {
        cb(err, null);
    });
    command.stdout.on("data", msg => {
        msg = msg.toString();
        const firstIndexOfBracket = msg.indexOf("{");
        if(firstIndexOfBracket != -1) {
            const lastIndexOfBracket = msg.lastIndexOf("}");
            const statsString = msg.substr(firstIndexOfBracket, lastIndexOfBracket);
            const statsObject = JSON.parse(statsString);
            if(statsObject.status)
                cb(null, statsObject.status.ansible_facts.nginx_status_facts);
            else
                cb(new Error("Request time out check the load balancer working status"), null);
        }
    });
}
module.exports.synchronizeConfiguration = sync;
module.exports.getServerStatistics = getStats;